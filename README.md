# Ce programme converti en gruger de box votre raspberrypi !

Il installe et configure un client VPN, un point d'accès wifi et un par-feu en une seule commande.

Ce programme reprend la procédure pas à pas expliquée sur l'atelier aquilenet.fr : 
![atlier vpn aquilenet](https://atelier.aquilenet.fr/projects/aquilenet/wiki/Brique_vpn)


## COPIE DU SYSTÈME SUR UNE CARTE SD

Utilisez une carte SD neuve sinon re-formattez-la. Il est fortement conseillé d'utiliser une carte class A1 (ce doit être écrit dessus).

Téléchargez le système Raspbian Stretch Lite ici : 

![download raspbian](https://www.raspberrypi.org/downloads/raspbian/)

Vous pouvez vérifier sa somme de contrôle (ici la version d'avril 2018) :

```$ sha256sum 2018-11-13-raspbian-stretch-lite.img```

et comparer la sortie de cette commande avec la somme de contrôle sur la page de téléchargement.

Copiez l'image sur la carte SD avec la commande dd pour Linux ou MacosX (sinon ici une procédure mac :

![macosx installing images](https://www.raspberrypi.org/documentation/installation/installing-images/mac.md))

```$ sudo dd if=/chemin/vers/raspbian-stretch-lite.img of=/dev/sdX bs=4m conv=fsync status=progress ```

ATTENTION : /dev/sdX. Le 'X' est l'emplacement de votre carte SD. Ne faites pas l'erreur d'écraser un de vos disques ! Pour en être sûr :

- Ejectez la carte
- `$ ls /sys/block`
- Insérez la carte
- `$ ls /sys/block`

Vous savez à quel emplacement est la carte en comparant ces 2 commandes avant et après avoir insérer la carte.

Pour les utilisateurs windows : 

![creez-carte-sd-raspbian-raspberry-pi-windows](https://raspbian-france.fr/creez-carte-sd-raspbian-raspberry-pi-windows/)

## INSTALLATION

Le gruger requiert des bibliothèques python3.

### Installation de ressources sur votre ordinateur

- git pour télécharger le programme et le maintenir à jour.
- virtualenv pour créer un environnement virtuel, le programme sera installé dans une "boite étanche".
- python3-pip pour y installer les bibliothèques python3 requises.

#### Vous êtes sous Debian ou Ubuntu

```$ sudo apt install git virtualenv python3-pip```

#### Vous êtes sous MacOSX

Vérifiez si python3 est présent :

```$ python3 --version```

Si cette commande ne retourne pas : Python 3.5.x (x est un numéro) alors il faut l'installer avec brew, avez-vous brew ?

```$ brew --version```

Si cette commande ne retourne pas de numéro de version, installez brew :

![brew](https://brew.sh/)

```$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"```

Et maintenant installez python3 et git :

```$ brew install python3 git```

pip est installé avec python3, installez virtualenv par pip

```$ pip install virtualenv```

#### Vous êtes sous Windows

Avez-vous python3 ? Ouvrez le terminal windows et tapez :

```$ python3 --version```

Si cette commande ne retourne pas de numéro de version, téléchargez et installez la dernière version de python3 ici : https://www.python.org/getit/windows/ et cliquez ci lien lien qui mène à la dernière version 3 (surtout pas python2.7 !)

pip est installé avec python3, installez virtualenv par pip

```$ pip install virtualenv```

Téléchargez et installez git : https://gitforwindows.org/

### Cloner le projet gruger avec git

```$ git clone git@framagit.org:tazogil/gruger.git```

Ultérieurement vous pourrez mettre à jour ce programme par :

```$ git clone git@framagit.org:tazogil/gruger.git```

### Créer l'environnement virtuel

Ouvrez un terminal et tapez les commandes suivants (Linux et MacosX) qui positione dans le répertoire du projet, créé un répertoire pour l'environnement et transforme ce répertoire ... en environnement virtuel python3.

```$ cd gruger```

```$ mkdir env```

```$ virtualenv -p /usr/bin/python3 env```

Note : pour windows ou mac, chercher l'emplacement de l'exe python3

### Démarrer l'environnement virtuel

```$ source ./env/bin/activate```


### Installer les libraries

Votre prompt commence maintenant par le nom de l'environnement (env).

pip installe fabric (https://www.fabfile.org/) et ses dépendances :
- invoke (https://www.pyinvoke.org/)
- paramiko (https://www.paramiko.org/)

```(env)$ pip install fabric```

# CONFIGURATION

Renommez le fichier fabric.yaml.tpl en fabric.yaml et complétez comme indiqué par les commentaires. 

ATTENTION : veillez à respecter l'indentation de ce fichier !

# UTILISATION

Si ce n'est pas le cas ouvrez un terminal, entrez dans le répertoire du projet gruger
et démarrez l'environnement virtuel :

```$ source ./env/bin/activate```

Pour lister les commandes disponibles :

```(env) fab -l```

L'installation se passe en 3 étapes séparées par 3 redémarrages du raspberry. À la fin de chaque étape, il vous sera demandé de poursuivre l'installation en passant à l'étape suivante une trentaine de seconde après, le temps que le système du raspberry soit prêt.

Lancement de l'étape 1 :

```(env)$ fab install```

Lancement de l'étape 2 :

```(env)$ fab install --step=2```

Lancement de l'étape 3 :

```(env)$ fab install --step=3```

Pour simplement voit les commandes sans les exécuter :

```(env) fab install --show```

```(env) fab install --step=2 --show```

```(env) fab install --step=3 --show```

Les autres commandes arrête le système, le redémarre, le met à jour et test le VPN.
