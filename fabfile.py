from fabric import Connection, Config
from fabric.tasks import task
from invoke import Responder
from os.path import splitext, basename, exists
from os import remove
from paramiko.ssh_exception import NoValidConnectionsError, SSHException
from termcolor import cprint
import cmd
import socket
import warnings
from getpass import getpass


#  https://github.com/ansible/ansible/issues/52598
warnings.filterwarnings(action='ignore', module='.*paramiko.*')

RASPBERRY_PI = "raspberrypi"
PI_USER = "pi"
PI_USER_PASSWORD = "raspberry"
TMPIP = ".tmpip"
c = None  # object de connexion
do_run = True  # si False les commandes ne sont pas exécutées


class ConfirmDialog(cmd.Cmd):
    "Invite de commande"

    def do_oui(self, line):
        return True

    def do_non(self, line):
        return True

    def do_EOF(self, line):
        return True


def ifconfig(ctx):
    "retourne les ips du PC"
    result = ctx.run("ifconfig", hide="out")
    addrs = []
    if result.ok:
        lines = result.stdout.split("\n")
        for line in lines:
            if "inet addr"in line:
                addr = line.strip()
                addr = addr[10:]
                p = addr.index(" ")
                addr = addr[:p]
                if addr != "127.0.0.1":
                    addrs.append(addr)
        return addrs


def nmap(ctx, network):
    "retourne l'IP d'un appareil connecté au réseau `network`"
    "ayant pour nom raspberry-pi"
    result = ctx.run("nmap -sP {0}".format(network), hide="out")
    if result.ok:
        lines = result.stdout.split("\n")
        for line in lines:
            if RASPBERRY_PI in line:
                p1 = line.index("(") + 1
                p2 = line.rindex(")")
                return line[p1:p2]


def _connexion(ctx, **kw):
    """Connexion au gruger.
    Si kw["ip"], c'est alors la première connexion par le
    réseau de la box lors de l'installation (invoquée par _detect).
    Le mot de passe est celui par défaut pour step1 d'install,
    invite du mot de passe pour step2 et 3 (arg ask_pass=True) car il a été
    changé à step1.

    Si "ip" est nulle c'est une connexion par l'IP statique qui a été attribué
    lors de l'installation ( cf static_ip). Le mot de passe est saisi à
    l'invite de commande et est enregistré dans Config.
    """
    global c
    ip = kw.get("ip", None)
    ask_pass = kw.get("ask_pass", False)
    if not do_run:
        return
    if ip is not None:
        host = ip
        sudo_pass = getpass("Entrez le mot de passe de 'pi' : ")\
            if ask_pass else PI_USER_PASSWORD
    else:
        host = ctx["static_ip"]
        host = host[:-3]
        sudo_pass = getpass("Entrez le mot de passe de 'pi' : ")
        ctx["sudo_gruger"] = sudo_pass
    cprint("Connexion à {0}".format(host), "yellow")
    config = Config(overrides={'sudo': {'password': sudo_pass}})
    c = Connection(host=host,
                   user=PI_USER,
                   connect_timeout=5,
                   config=config,
                   connect_kwargs={"password": sudo_pass,
                                   "look_for_keys": False})
    try:
        c.sudo("whoami", hide="out")
    except socket.timeout:
        cprint("""L'hôte {0} ne répond pas.
               Etes-vous sûr d'être connecté au wifi du grugeur ?""".format(
                   host), "red", attrs=["bold"])
        exit()
    except NoValidConnectionsError:
        cprint("L'hôte {0} ne répond pas.".format(host),
               "red", attrs=["bold"])
        cprint("Le grugeur est-il allumé ? Est-il connecté à la box ?"
               "Votre connexion est-elle valide ?", "red")
        exit()
    except SSHException:
        cprint("session SSH ou mot de passe invalide",
               "red", attrs=["bold"])
        exit()
    else:
        cprint("Vous êtes connecté à {0}".format(host), "green")


def p_run(command, color="yellow"):
    """imprime en couleur la commande et exécute sans sudo.
    """
    cprint(command, color)
    if do_run:
        return c.run(command)


def p_sudo(command, color="yellow"):
    """imprime en couleur la commande et l'exécute en sudo.
    """
    cprint("sudo {0}".format(command), color)
    if do_run:
        return c.sudo(command)


def _install_packages(ctx):
    "installation des logiciels"
    cprint("Installation de openvpn hostapd dnsmasq", "yellow", attrs=["bold"])
    p_sudo("apt install -q -y openvpn hostapd dnsmasq")


def _set_static_ip(ctx):
    "affectation d'une IP fixe"
    cprint("Configuration du point d'accès wifi", "yellow", attrs=["bold"])
    cprint("Mise en place d'une IP statique sur l'interface WiFi", "yellow")
    dhcpd_file = "templates/dhcpd.conf.tpl"
    if do_run:
        with open(dhcpd_file, "r") as fh:
            t = fh.read()
            t = t.format(static_ip=ctx["static_ip"])
            with open(dhcpd_file, "w") as fh:
                fh.write(t)
    cprint("Transfert de templates/dhcpd.conf.tpl", "yellow")
    if do_run:
        c.put("templates/dhcpd.conf.tpl", "/tmp/")
    p_sudo("mv /tmp/dhcpd.conf.tpl /etc/dhcpcd.conf")
    p_sudo("service dhcpcd restart")
    p_sudo("systemctl daemon-reload")
    p_sudo("service dhcpcd restart")


def _configure_dhcpd(ctx):
    "configuration dhcp"
    cprint("Configuration du serveur DHCP", "yellow", attrs=("bold",))
    cprint("Transfert de templates/dnsmasq.conf.tpl", "yellow")
    if do_run:
        c.put("templates/dnsmasq.conf.tpl", "/tmp/")
    p_sudo("mv /tmp/dnsmasq.conf.tpl /etc/dnsmasq.conf")


def _configure_hostapd(ctx):
    "configuration du point d'accès wifi"
    cprint("Configuration de hostapd", "yellow", attrs=("bold",))
    hostapd_file = "templates/hostapd.conf.tpl"
    cprint("Transfert de templates/hostapd.conf.tpl", "yellow")
    if do_run:
        with open(hostapd_file, "r") as fh:
            t = fh.read()
            t = t.format(ssid=ctx["ssid"], wpa_passphrase=ctx["wpa_passphrase"])
            with open(hostapd_file, "w") as fh:
                fh.write(t)
        c.put("templates/hostapd.conf.tpl", "/tmp/")
    p_sudo("mv /tmp/hostapd.conf.tpl /etc/hostapd/hostapd.conf")
    s = '#DAEMON_CONF=""'
    r = 'DAEMON_CONF="/etc/hostapd/hostapd.conf"'
    p_sudo("sed -i 's:{s}:{r}:' /etc/default/hostapd".format(s=s, r=r))
    cprint("Démarrage du hotspot wifi", "yellow", attrs=("bold",))
    p_sudo("systemctl unmask hostapd")
    p_sudo("systemctl start hostapd")
    p_sudo("systemctl enable hostapd")


def _configure_vpn(ctx):
    "configuration du vpn"
    cprint("Configuration du VPN", "yellow", attrs=["bold"])
    ovpn_path = ctx["ovpn"]
    file_name = basename(ovpn_path)
    name, ext = splitext(file_name)
    cprint("Transfert de {0}".format(ovpn_path), "yellow")
    if do_run:
        c.put(ovpn_path, "/tmp")
    p_sudo("mv /tmp/{name}{ext} /etc/openvpn/{name}.conf".format(
        name=name, ext=ext))
    cprint("Création du fichier 'secret' de l'identifiant et"
           "du mot de passe du VPN", "yellow")
    cmd_2 = "touch /tmp/secret && echo '{0}' > /tmp/secret".format(
        ctx["vpn_login_user"])
    cmd_2 += "&& echo '{0}' >> /tmp/secret".format(
        ctx["vpn_login_passphrase"])
    p_run(cmd_2)
    p_sudo("mv /tmp/secret /etc/openvpn/secret")
    p_sudo("chmod 600 /etc/openvpn/secret")
    cmd_5 = "sudo sed -i 's/auth-user-pass/auth-user-pass secret/' "
    cmd_5 += "/etc/openvpn/{name}.conf".format(name=name)
    p_sudo(cmd_5)
    p_sudo("systemctl start openvpn@{name}".format(name=name))


def _configure_firewall(ctx):
    "Configuration du pare-feu"
    cprint("Configuration du pare-feu", "yellow", attrs=["bold"])
    cmd_1 = "sudo sed -i 's/#{net}/{net}/' /etc/sysctl.conf".format(
        net="net.ipv4.ip_forward=1")
    p_sudo(cmd_1)
    cmd_2 = "sudo sed -i 's/#{net}/{net}/' /etc/sysctl.conf".format(
        net="inet.ipv6.conf.all.forwarding=1")
    p_sudo(cmd_2)
    p_sudo("sysctl --system")
    cprint("Transfert de templates/netfilter.tpl", "yellow")
    if do_run:
        c.put("templates/netfilter.tpl", "/tmp")
    p_sudo("mv /tmp/netfilter.tpl /etc/init.d/netfilter")
    p_sudo("chmod +x /etc/init.d/netfilter")
    p_sudo("/etc/init.d/netfilter start")
    p_sudo("update-rc.d netfilter defaults")
    p_sudo("update-rc.d netfilter enable")


def _detect(ctx):
    """Cherche l'IP du raspberry connecté au réseau de la box pendant
    l'installation et lance une connexion.
    L'étape 1 de l'installe trouve l'IP et l'écrit dans TMPIP les 2 autres
    étapes la trouve dans ce fichier.
    """

    if exists(TMPIP):  # step>1
        with open(TMPIP, "r") as fh:
            ip = fh.read().strip()
        kw = {"ip": ip, "ask_pass": True}
        _connexion(ctx, **kw)
    else:  # step=1
        cprint("détecter la raspberry-pi sur le réseau local de la box",
               "yellow", attrs=("bold",))
        if not do_run:
            cprint("ifconfig et nmap -sp plage-ip-réseau-local", "yellow")
            return
        ip = None
        addrs = ifconfig(ctx)
        addrs = [a[:a.rindex(".")] + ".*"for a in addrs]
        for addr in addrs:
            ip = nmap(ctx, addr)
        if ip is None:
            cprint("Impossible de trouver un {0} sur {1} ".format(
                RASPBERRY_PI, "ou ".join(addrs)), "red")
            exit()
        kw = {"ip": ip}
        with open(TMPIP, "w") as fh:
            fh.write(ip)
        _connexion(ctx, **kw)


def _control_config(ctx):
    "Contrôle si le fichier de configuration fabric.yaml est bien renseigné."
    error = False
    ssid = ctx.get("ssid", None)
    if ssid is None:
        error = True
        cprint("Vous n'avez pas renseigné l'identifiant du hotspot WIFI.",
               "red", attrs=["bold"])
        cprint("Voir 'ssid' dans fabric.yaml ", "red")

    wpa = ctx.get("wpa_passphrase", None)
    if wpa is None:
        error = True
        cprint("Vous n'avez pas renseigné le mot de passe du hotspot WIFI.",
               "red", attrs=["bold"])
        cprint("Voir 'wpa_passphrase' dans fabric.yaml ", "red")

    vpn = ctx.get("vpn_login_user", None)
    if vpn is None:
        error = True
        cprint("Vous n'avez pas renseigné le login de l'abonnement VPN.",
               "red", attrs=["bold"])
        cprint("Voir 'vpn_login_user' dans fabric.yaml ", "red")

    vpn_pass = ctx.get("vpn_login_passphrase", None)
    if vpn_pass is None:
        error = True
        cprint("Vous n'avez pas renseigné le mot de passe de l'abonnement VPN.",
               "red", attrs=["bold"])
        cprint("Voir 'vpn_login_passphrase' dans fabric.yaml ", "red")

    ovpn = ctx.get("ovpn", None)
    if ovpn is None:
        error = True
        cprint("Vous n'avez pas renseigné le chemin vers le fichier .ovpn.",
               "red", attrs=["bold"])
    elif not exists(ovpn):
        error = True
        cprint("Le chemin vers le fichier .ovpn est invalide.",
               "red", attrs=["bold"])

    static_ip = ctx.get("static_ip", None)
    if static_ip is None:
        error = True
        cprint("Vous n'avez pas renseigné l'ip statique qui sera attribué "
               "au {0}. Voir static_p dans fabric.yaml ".format(GRUGEUR),
               "red", attrs=["bold"])
    else:
        addrs = ifconfig(ctx)
        if static_ip in addrs:
            error = True
            cprint("L'ip statique {0} fait déja partie "
                   "d'un réseau local utilisé chez vous. Les choix "
                   "sont 10.0.0.* ou 172.16.10.* ou 192.168.*".format(
                       static_ip), "red", attrs=["bold"])
    if not error:
        cprint("Le fichier de configuration fabric.yaml est bien renseigné",
               "green", attrs=("bold",))
    else:
        exit()


def _change_pi_password(ctx):
    "Changement du mot de passe de l'utilisateur pi"
    old_pass = ctx.get("sudo_gruger", None)
    if old_pass is None:
        old_pass = PI_USER_PASSWORD
    oldpass = Responder(pattern=r'\(current\) UNIX password:',
                        response=old_pass+'\n',)
    cprint("changement du mot de passe de l'utilisateur 'pi",
           "yellow", attrs=["bold"])
    cprint("passwd (current) UNIX password: ########", "yellow")
    cprint("passwd Enter new UNIX password: ########", "yellow")
    cprint("passwd Retype new UNIX password: ########", "yellow")
    if do_run:
        c.run('passwd', pty=True, watchers=[oldpass])


def _update(ctx):
    if c is None:
        _connexion(ctx)
    cprint("Mise à jour du système", "yellow", attrs=["bold"])
    p_sudo("apt update -q")
    p_sudo("apt upgrade -q -y")


def _test_vpn(ctx):
    if c is None:
        _connexion(ctx)
    cprint("Test du VPN", "yellow", attrs=["bold"])
    ovpn_path = ctx["ovpn"]
    file_name = basename(ovpn_path)
    name, ext = splitext(file_name)
    result = p_sudo("systemctl status openvpn@{0}".format(name))
    if result:
        if result.ok:
            cprint("Le VPN est démarré", "green", attrs=["bold"])
        else:
            cprint("Le VPN est ne démarre pas.", "red", attrs=["bold"])
            cprint("Etes-vous sûr de vos identifiant et mot de passe ? "
                   "Etes-vous à jour dans votre abonnement ?", "red")


@task
def install(ctx, step=1, show=False):
    "installation --step=1|2|3 --show affiche les commandes sans exécution."
    global do_run
    if step == 1:
        if show:
            cprint("Liste les commandes sans les exécuter",
                   "green", attrs=("bold",))
            do_run = False
        else:
            confirm = ConfirmDialog()
            confirm.prompt = """Confirmez-vous l'installation ?
            'oui' pour confirmer, sinon 'non' : """
            confirm.cmdloop()
            if confirm.lastcmd == "non":
                cprint("""Installation suspendue.
        $ fab install --show pour lister les commandes sans les exécuter.""",
                       "green")
                exit()
        _detect(ctx)
        _change_pi_password(ctx)
        _update(ctx)
        _install_packages(ctx)
        cprint("""Premier redémarrage nécessaire. Dans 30 secondes,
               continuez l'installation par cette commande :
               fab install --step=2""",
               "yellow", attrs=("bold",))
        p_sudo("reboot")

    elif step == 2:
        if show:
            cprint("Liste les commandes sans les exécuter",
                   "green", attrs=("bold",))
            do_run = False
        _detect(ctx)
        _set_static_ip(ctx)
        _configure_dhcpd(ctx)
        _configure_hostapd(ctx)
        cprint("""Second redémarrage nécessaire. Dans 30 secondes,
               finissez l'installation par cette commande :
               fab install --step=3""",
               "yellow", attrs=("bold",))
        p_sudo("reboot")
    elif step == 3:
        if show:
            cprint("Liste les commandes sans les exécuter",
                   "green", attrs=("bold",))
            do_run = False
        _detect(ctx)
        _configure_vpn(ctx)
        _configure_firewall(ctx)
        cprint("Dernier redémarrage !", "yellow", attrs=("bold",))
        cprint("Vous pouvez vous connecter en wifi sur {0} dans 30 secs".format(
            ctx["ssid"]), "green", attrs=("bold",))
        if exists(TMPIP):
            remove(TMPIP)


@task
def halt(ctx):
    "Arrêter le système"
    confirm = ConfirmDialog()
    confirm.prompt = """Confirmez-vous l'arrêt du grugeur de box ?
    'oui' pour confirmer, sinon 'non' : """
    confirm.cmdloop()
    if confirm.lastcmd == "oui":
        cprint("Arrêt du grugeur", "green", attrs=["bold"])
        _connexion(ctx)
        p_sudo("halt")
    else:
        cprint("Arrêt du système annulé", "green")
    exit()


@task
def reboot(ctx):
    "Redémarrer le système du grugeur"
    confirm = ConfirmDialog()
    confirm.prompt = """Confirmez-vous le redémarrage du grugeur de box ?
    'oui' pour confirmer, sinon 'non' : """
    confirm.cmdloop()
    if confirm.lastcmd == "oui":
        cprint("Redémarrage du grugeur", "green", attrs=["bold"])
        _connexion(ctx)
        p_sudo("reboot")
    else:
        cprint("Redémarrage annulé", "green")
        exit()


@task
def test_vpn(ctx):
    "Test de bon fonctionnement du VPN "
    _test_vpn(ctx)


@task
def update(ctx):
    "Mise à jour du système."
    _update(ctx)


@task
def password(ctx):
    "Changement du mot de passe de l'utilisateur pi"
    _connexion(ctx)
    _change_pi_password(ctx)
