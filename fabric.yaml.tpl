  # FICHIER DE CONFIGURATION DU GRUGER
static_ip:  # la plage d'adresses IP a attribuer au gruger
  172.16.10.1/24 ou 10.0.0.1/24
ssid:  # le nom du point d'accès WIFI
  mon_ssid
wpa_passphrase: # le mot de passe du ssid
  le_mot_de_passe_de_mon_ssid
ovpn:  # le fichier de configuration du vpn fourni par votre abonnement
  /le/chemin/vers/le/fichier/.ovpn
vpn_login_user: # le login de votre abonnement au serveur VPN
  login_du_vpn
vpn_login_passphrase: # le mot de pass de votre abonnement au serveur VPN
  mot_de_passe_du_vpn
