#!/bin/bash
# Script Netfilter
### BEGIN INIT INFO
# Provides: netfilter
# Required-Start:
# Required-Stop:
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Start firewall daemon at boot time
# Description: Custom Firewall scrip.
### END INIT INFO

case "$1" in
'start')
# On vide les anciennes règles
/sbin/iptables -F
/sbin/iptables -X
/sbin/iptables -Z

/sbin/ip6tables -F
/sbin/ip6tables -X
/sbin/ip6tables -Z

# Mise en place des sécurités noyaux
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_broadcasts
echo 1 > /proc/sys/net/ipv4/tcp_syncookies
echo 1 > /proc/sys/net/ipv4/conf/all/log_martians
echo 1 > /proc/sys/net/ipv4/icmp_ignore_bogus_error_responses
echo 1 > /proc/sys/net/ipv4/conf/all/rp_filter
echo 0 > /proc/sys/net/ipv4/conf/all/accept_redirects
echo 0 > /proc/sys/net/ipv4/conf/all/send_redirects

# Mise en place du NAT pour permettre au réseau Wi-Fi de communiquer sur Internet
iptables -t nat -A POSTROUTING -o tun0 -s 172.16.10.0/24 -j MASQUERADE
# Par défaut, on bloque les paquets provenant d'un réseau vers un autre réseau
# On bloque ce qui arrive sur le Raspberry
# On autorise tout ce qui sort du boîtier
iptables -t filter -P FORWARD DROP
iptables -t filter -P OUTPUT ACCEPT
iptables -t filter -P INPUT DROP
ip6tables -t filter -P FORWARD DROP
ip6tables -t filter -P OUTPUT ACCEPT
ip6tables -t filter -P INPUT DROP

# On autorise toutes les connexions en localhost (boucle locale)
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT
ip6tables -t filter -A INPUT -i lo -j ACCEPT
ip6tables -t filter -A OUTPUT -o lo -j ACCEPT

# On autorise les réponses à une connexion préalablement établie
iptables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -t filter -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -t filter -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -t filter -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -t filter -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -t filter -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# On autorise tout type de flux provenant du réseau Wi-Fi à destination d'internet
iptables -t filter -A FORWARD -i wlan0 -o tun0 -s 172.16.10.0/24 -m state --state NEW -j ACCEPT

# On autorise les machines connectées au réseau Wi-Fi à se connecter sur le boîtier
iptables -t filter -A INPUT -i wlan0 -m state --state NEW -j ACCEPT
ip6tables -t filter -A INPUT -i wlan0 -m state --state NEW -j ACCEPT

# On autorise les requêtes RA et NDP vers mon Raspberry PI
ip6tables -A INPUT -p icmpv6 --icmpv6-type neighbour-solicitation -j ACCEPT
ip6tables -A INPUT -p icmpv6 --icmpv6-type neighbour-advertisement -j ACCEPT
ip6tables -A INPUT -p icmpv6 --icmpv6-type router-advertisement -j ACCEPT

sleep 3
echo -e "Starting firewall.. \n"
;;
'stop')
/sbin/iptables -F
/sbin/iptables -t nat -F
/sbin/iptables -t mangle -F
/sbin/iptables -P INPUT ACCEPT
/sbin/iptables -P FORWARD ACCEPT
/sbin/iptables -P OUTPUT ACCEPT

/sbin/ip6tables -F
/sbin/ip6tables -P INPUT ACCEPT
/sbin/ip6tables -P FORWARD ACCEPT
/sbin/ip6tables -P OUTPUT ACCEPT

sleep 3
echo -e "Stopping firewall.. \n"
;;
*)
;; 
esac
exit
